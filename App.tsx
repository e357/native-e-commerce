import { createNativeStackNavigator, NativeStackNavigationProp } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import {   OptionsScreen } from './Screen/OptionsScreen';
import { ProductManagement } from './Component/ProductManagement';
import { AddProduct } from './Component/AddProduct';
import { HomeScreen } from './Screen/HomeScreen';
import { Provider } from 'react-redux';
import { store } from './App/store';


//add plugin on babel to use the . env
//otherwise add it on the app.json
//expo constante
const Stack = createNativeStackNavigator();

export interface NavProps {
  navigation: NativeStackNavigationProp<any>
}

export default function App() {
  return (
    <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
        <Stack.Screen name="HomeScreen" component={HomeScreen}/>
        <Stack.Screen name="OptionsScreen" component={OptionsScreen} />
        <Stack.Screen name="ProductManagement" component={ProductManagement}/>
        <Stack.Screen name="AddProduct" component={AddProduct}/>
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  );
}
