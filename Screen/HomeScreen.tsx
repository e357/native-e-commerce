import {  Text, TouchableOpacity, View } from "react-native";
import { NavProps } from "../App";
import React from 'react';






export function HomeScreen ({navigation}: NavProps){

    return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity onPress={()=> navigation.navigate("OptionsScreen")} >
                <Text>Options</Text>
            </TouchableOpacity>
        </View>
    )
}