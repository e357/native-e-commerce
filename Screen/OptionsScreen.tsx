import { Button, Text, View } from "react-native";
import React from 'react';
import { NavProps } from "../App";
import { Icon } from 'react-native-elements';
import { styles } from "../Component/style";



export function OptionsScreen({ navigation }: NavProps) {

    return (
        <View style={{ flex: 1, alignItems: 'center',marginTop: 30 }}>

            <Text style={styles.titles}>Options Screen</Text>

            <View >
                <Button title="Product Management" onPress={() => navigation.navigate("ProductManagement")} />
            </View>

            <View style={{ margin: 20 }}>
                <Button title="Users Management" onPress={() => navigation.navigate("ProductManagement")} />
            </View>
            <View >
                <Button title="Orders Management" onPress={() => navigation.navigate("ProductManagement")} />
            </View>
        </View>
    )
}

