

export interface User{
    id?: string;
    name?: string;
    email?: string;
    password?: string;
    address?: string;
    avatar?: string;
    phone?: string;
}


export interface Produit{
    id?: number;
    name?: string;
    category?: string;
    price?: number;
    stock?: number;
    description?: string;
    picture?: string;
}