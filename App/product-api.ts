import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'
import { Produit } from './entities'
import Constants from 'expo-constants';


export const productApi= createApi ({
    reducerPath: 'productApi',
    tagTypes:['produit'],
    baseQuery: fetchBaseQuery({baseUrl: Constants.manifest?.extra?.SERVER_URL+ 'api/product'}),
    endpoints: (builder) =>({
        postProduct: builder.mutation<Produit, Produit>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['produit']
        })
    })
})


export const {usePostProductMutation} = productApi