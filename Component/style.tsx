

import { StyleSheet } from 'react-native';



export const styles = StyleSheet.create({
    titles: {
        fontSize: 20,
        margin: 10
    },
    input: {
        padding: 2,
        margin: 3,
        backgroundColor: '#3399ff',
        color: 'white'
        
    }
});