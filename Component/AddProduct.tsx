import React from "react";
import { Text, View, TextInput, Button, Alert, StyleSheet } from "react-native";
import { useForm, Controller } from "react-hook-form";

import { Produit } from "../App/entities";
import { usePostProductMutation } from "../App/product-api";




interface Props {
    produit: Produit;
}


export function AddProduct() {
    const { control, handleSubmit, formState: { errors } } = useForm();
    const [postForm, { isLoading, data, error, isSuccess }] = usePostProductMutation()
    const onSubmit = (data: Produit) => postForm(data);




    return (
        <View style={{ flex: 1, alignItems: 'center', marginTop: 30 }}>

            <Text >Add Product </Text>

            <View style={{ margin: 5 }}>
                <Controller
                    control={control}
                    rules={{
                        required: true,
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Name"

                        />
                    )}
                    name="Name"
                    defaultValue=""

                />
                {errors.Name && <Text>This is required.</Text>}

            </View>


            <View style={{ margin: 5 }}>
                <Controller
                    control={control}
                    rules={{
                        required: true,
                        maxLength: 100,
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Category"
                        />
                    )}
                    name="Category"
                    defaultValue=""
                />
                {errors.Category && <Text>This is required.</Text>}

            </View>

            <View style={{ margin: 5 }}>
                <Controller
                    control={control}
                    rules={{
                        required: true,
                        maxLength: 100,
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            keyboardType={'numeric'}
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Price"
                        />
                    )}
                    name="Price"
                    defaultValue=""
                />
                {errors.Price && <Text>This is required.</Text>}
            </View>

            <View style={{ margin: 5 }}>
                <Controller
                    control={control}
                    rules={{
                        required: true,
                        maxLength: 100,

                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}

                        />
                    )}
                    name="Stock"
                    defaultValue="1"
                />
                {errors.Stock && <Text>This is required.</Text>}

            </View>

            <View style={{ margin: 5 }}>
                <Controller
                    control={control}
                    rules={{
                        required: true,
                        maxLength: 100,
                    }}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.input}
                            onBlur={onBlur}
                            onChangeText={onChange}
                            value={value}
                            placeholder="Picture"
                        />
                    )}
                    name="Picture"
                    defaultValue=""
                />
                {errors.Picture && <Text>This is required.</Text>}
            </View>
            <View style={{ margin: 5 }}>
                <Button title="Submit" onPress={handleSubmit(onSubmit)} />
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    label: {
        color: 'white',
        margin: 20,
        marginLeft: 0,
    },
    button: {
        marginTop: 40,
        color: 'white',
        height: 40,
        backgroundColor: '#ec5990',
        borderRadius: 4,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 8,
        backgroundColor: '#0e101c',
    },
    input: {
        backgroundColor: 'white',
        borderColor: 'none',
        height: 40,
        padding: 10,
        borderRadius: 4,
    },
});


// export default function App() {
//     const { register, handleSubmit, formState: { errors } } = useForm();
//     const onSubmit = (data: Produit) => console.log(data);
//     console.log(errors);

//     return (
//         <form onSubmit={handleSubmit(onSubmit)}>
//             <input type="text"
//                 placeholder="Name" {...register("Name", { required: true, maxLength: 80 })} />
//             <textarea {...register("Category", { required: true })} />

//             <input type="number"
//                 placeholder="Price" {...register("Price", { required: true, pattern: /^\S+@\S+$/i })} />
//             <textarea {...register("Description", { required: true })} />
//             <select {...register("Picture", { required: true })}>
//             </select>

//             <input type="submit" />
//         </form>
//     );
// }





// const initalState={
//     Name: '',
//     Category: '',
//     Price: '',
//     Stock:'',
//     Description: '',
//     Picture: ''
// }
//  const [ initialState, setState]= useState('')



//     const handleSubmit = async ()=>{

//     }

// interface Props {
//     produit: Produit;
// }


// const [form, setForm] = useState <Produit> ({} as Produit)
// const [postProduct] = usePostProductMutation()
// const formref: LegacyRef<any> = useRef()

// const handleFile = async(event: React.FormEvent<EventTarget>)=>{
//     let target= event.target as HTMLInputElement;
//     let placeholder= target.placeholder;
//     let value = target.files?.item(0);
//     let change= {...form, [placeholder]: value }

//     console.log(setForm(change));

//     setForm(change)
// }

// const handleSubmit= async(event: React.FormEvent<EventTarget>)=>{
//     try {
//         event.preventDefault()

//         const formData = new FormData(formref.current);

//         const send = await postProduct({body: formData, id: produit.id}).unwrap()
//     } catch (error:any) {
//         console.log(error.data);


//     }
// }
// 