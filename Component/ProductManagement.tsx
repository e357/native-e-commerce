import { Button, Text, View } from "react-native";
import React from 'react';
import { NavProps } from "../App";
import { Icon } from 'react-native-elements'
import { styles } from "./style";



export function ProductManagement({ navigation }: NavProps) {


    return (
        <View style={{ flex: 1, alignItems: 'center', marginTop: 30 }}>

            <Text style={styles.titles}>Product Management</Text>

            <View>
                <Button title="Add Product" onPress={() => navigation.navigate("AddProduct")} />
            </View>
            <View style={{ margin: 20 }}>
                <Button title="Update Product" onPress={() => navigation.navigate("AddProduct")} />
            </View>
            <View >
                <Button title="Delete Product" onPress={() => navigation.navigate("AddProduct")} />
            </View>

        </View>
    )
}